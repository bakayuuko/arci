#!/bin/env bash

echo "User name:"
read USER
echo "Password:"
read PASSWORD

# Prepare disk
yes | mkfs.ext4 /dev/sda1
mount /dev/sda1 /mnt
yes | mkswap /dev/sda2
swapon /dev/sda2

# sync time:
timedatectl set-ntp true

# update mirrorlist 
pacman -Sy pacman-contrib --noconfirm
echo "--Ranking mirrors--"
# change Arch Linux repo to Indonesia repository
curl -s "https://www.archlinux.org/mirrorlist/?country=ID&protocol=http&use_mirror_status=on" | sed -e 's/^#Server/Server/' -e '/^#/d' | rankmirrors -n 6 - > /etc/pacman.d/mirrorlist

pacman -Syy 
pacstrap /mnt base base-devel sudo grub os-prober
genfstab -U /mnt >> /mnt/etc/fstab

chroot_actions(){
    
    # locale
    echo 'LANG="en_US.UTF-8"' >> /etc/locale.conf
    echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
    echo "id_ID.UTF-8 UTF-8" >> /etc/locale.gen
    locale-gen
    ln -sf /usr/share/zoneinfo/Asia/Jakarta /etc/localtime
    hwclock --systohc

    # hosts
    hostname=nanolab
    echo $hostname > /etc/hostname
    echo "127.0.0.1 localhost.localdomain localhost" > /etc/hosts
    echo "::1       localhost.localdomain localhost" >> /etc/hosts
    echo "127.0.1.1 $hostname.localdomain $hostname" >> /etc/hosts

    # users
    user="$1"
    password="$2"
    echo -en "$password\n$password" | passwd
    useradd -m -G wheel,users -s /bin/bash "$user"
    echo -en "$password\n$password" | passwd "$user"
    echo 'root ALL=(ALL) ALL' > /etc/sudoers
    echo '%wheel ALL=(ALL) ALL' >> /etc/sudoers

    # coding
    pacman -S git --noconfirm
    pacman -S python python2 --noconfirm

    pacman -S zsh --noconfirm
    chsh -s /usr/bin/zsh
    
    # server pkg
    pacman -S dhcp --noconfirm
    systemctl enable dhcpd4.service
    pacman -S tftp-hpa --noconfirm
    systemctl enable tftpd.service
    pacman -S nfs-utils --noconfirm
    systemctl enable nfs-server.service

    # cli programming
    pacman -S go go-tools ruby --noconfirm

    # grub
    grub-install --recheck --target=i386-pc /dev/sda
    grub-mkconfig -o /boot/grub/grub.cfg

    user_actions(){
        chsh -s /usr/bin/zsh $user
    }
    export -f user_actions
    su "$user" -c "bash -c user_actions"
}

export -f chroot_actions
arch-chroot /mnt /bin/bash -c "chroot_actions $USER $PASSWORD"


echo "Press any key to reboot."
read pause
umount -R /mnt
reboot